<?php

namespace Login\LoginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Login\LoginBundle\Entity\Users;
use Login\LoginBundle\Modals\Login;

class DefaultController extends Controller {

        public function indexAction(Request $request) {
                $session = $this->getRequest()->getSession();
                $em = $this->getDoctrine()->getEntityManager();
                $repository = $em->getRepository('LoginLoginBundle:Users');

                if ($request->getMethod() == 'POST') {
                        $session->clear();

                        $username = $request->get('username');
                        $password = sha1($request->get('password'));
                        $remember = $request->get('remember');


                        $user = $repository->findOneBy(array('userName' => $username, 'password' => $password));

                        if ($user) {
                                if ($remember == 'rem_me') {
                                        $login = new Login();
                                        $login->setUsername($username);
                                        $login->setPassword($password);
                                        $session->set('login', $login);
                                }
                                return $this->render('LoginLoginBundle:Default:welcome.html.twig', array('name' => $user->getUserName()));
                        } else {
                                return $this->render('LoginLoginBundle:Default:login.html.twig', array('error' => 'Error Login!!!'));
                        }
                } else {
                        if ($session->has('login')) {
                                $login = $session->get('login');
                                $username = $login->getUsername();
                                $password = $login->getPassword();
                                $user = $repository->findOneBy(array('userName' => $username, 'password' => $password));

                                if ($user) {
                                         return $this->render('LoginLoginBundle:Default:welcome.html.twig', array('name' => $user->getUserName()));
                                }
                        }
                                return $this->render('LoginLoginBundle:Default:login.html.twig');
                        }
                }

                public function signupAction(Request $request) {
                        if ($request->getMethod() == 'POST') {
                                $userName = $request->get('username');
                                $firstName = $request->get('firstname');
//                        $email = $request->get('email');
                                $password = sha1($request->get('passwd'));
                              
                                
                                $user = new Users();
                                $user->setFirstName($firstName);
                                $user->setUserName($userName);
                                $user->setPassword($password);

                                $em = $this->getDoctrine()->getEntityManager();
                                $em->persist($user);
                                $em->flush();
                        }
                        return $this->render('LoginLoginBundle:Default:login.html.twig');
                }
                
                public function logoutAction(Request $request)
                {
                        $session = $this->getRequest()->getSession();
                        $session->clear();
                        return $this->render('LoginLoginBundle:Default:login.html.twig');
                }

        }
        