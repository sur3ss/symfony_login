<?php

namespace Login\LoginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Login\FileBundle\Models\Document;

class HomeController extends Controller{
        public function uploadAction(Request $request){
                if($request->getMethod() == 'POST'){
                        $image = $request->files->get('img');
                        $status = 'success';
                        $uploadedURL = '';
                        $message = '';
                        if(($image instanceof UploadedFile) && ($image->getError() == '0')) 
                        {
                                if(!($image->getSize() < 20000)){
                                        $originalName = $image->getClientOriginalName();
                                        $name_array = explode('.', $originalName);
                                        $file_type = $name_array[sizeof($name_array)-1];
                                        $valid_filetypes = array('jpg', 'jpeg', 'bmp', 'png');
                                        if(in_array(strtolower($file_type), $valid_filetypes)){
                                                //start uploading file
                                                
                                                $document = new Document();
                                                $document->setFile($image);
                                                $document->setSubDirectory('uploads');
                                                $document->processFile();
                                                $uploadedURL = $uploadURL = $document->getUploadDirectory(). DIRECTORY_SEPARATOR . $image->getBasename();
                                        } else {
                                                $status = "failed";
                                                $message = "Invalid file type.";
                                                
                                        }
                                        
                                } else {
                                        $status = 'Failed';
                                        $message = "Size exceeds limit.";
                                }
                                
                        } else {
                                 $status = 'Failed';
                                 $message = "File Error";
                        }
                        return $this->render('LoginLoginBundle:Default:welcome.html.twig', array('name'=> 'AdminName', 'status' => $status, 'message' => $message, 'uploadedURL' => $uploadedURL));
                } else {
                        return $this->render('LoginLoginBundle:Default:welcome.html.twig');
                        
                }
        }
}
